# -*- coding: utf-8 -*-
import re
import scrapy
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode, quote


class SamplesSpider(scrapy.Spider):
    name = 'samples'
    allowed_domains = ['qrl.dell.com']
    start_urls = ['https://qrl.dell.com/Product/Categories']

    manuals = set()

    custom_settings = {
        'ITEM_PIPELINES': {'qrl.pipelines.CustomFilePipeline': 1},
    }


    def start_requests(self):
        # self.manuals_to_check = [_normalize(t) for t in self.manuals_to_check]
        # self.pages_to_save = [_normalize(t) for t in self.pages_to_save]

        for url in self.start_urls:
            yield scrapy.Request(url, callback=self.parse)


    def parse(self, response):
        urls = response.css('div.CategoryBlock a ::attr(href)').extract()

        for url in urls:
            yield response.follow(url, callback=self.parse_category)


    def parse_category(self, response):
        urls = response.css('ul.ul_ProductsCategory li a ::attr(href)').extract()

        for url in urls:
            yield response.follow(url, callback=self.parse_item)


    def parse_item(self, response):
        product = _strip(response.css('div#productPicHolder span.Span_productName ::text').extract_first())
        manuals = self.get_manuals(response)

        try:
            owner_manual = next(m for m in manuals if 'owners manual' in _normalize(m['name']))

            d = {'product': product, 'manual_name': owner_manual['name'], 'manual_url': owner_manual['url']}

            yield response.follow(owner_manual['url'], callback=self.find_memory_samples, meta={'item': d})
        except:
            pass


    def find_memory_samples(self, response):
        d = response.meta['item']
        base_url = response.meta.get('base_url', re.sub('/[^/]*$', '/', response.url))

        if d['manual_url'] in self.manuals:
            return

        links = self.get_links(response, base_url)

        for name, url in links:
            if 'sample memory configurations' in _normalize(name):
                self.manuals.add(d['manual_url'])

                d['page_name'] = name
                d['filename'] = _safe_filename('{} - {}.html'.format(d['product'], d['page_name']))
                d['file_urls'] = [url]

                yield d
                return
            else:
                yield response.follow(url, callback=self.find_memory_samples, meta={'item': d, 'base_url': base_url})


    def get_manuals(self, response):
        for li in response.css('div#Documents ul#ProductList1 li.li_product_document'):
            name = _strip(li.css('td.td_product_document_alignment center.span_product_document_name ::text').extract_first())
            url = _strip(li.css('td.td_product_document_alignment a ::attr(href)').extract_first())

            if url and not url.startswith('http'):
                url = response.urljoin(url)

            d = {'name': name, 'url': url}

            yield d


    def get_links(self, response, base_url):
        d = response.meta['item']

        for li in response.css('div#productList ul li'):
            name = _strip(li.css('a ::text').extract_first())
            name = re.sub('\s+', ' ', name)
            url = _strip(li.css('a ::attr(href)').extract_first())

            if url and not url.startswith('http'):
                url = response.urljoin(url)

            if url.startswith(base_url):
                yield (name, url)
            else:
                self.logger.info('Check <{}> at <{}>'.format(base_url, url))

        for div in response.css('div#mainDataHolder div.data_ProductInfo_ManualFreeHeight'):
            name = _strip(div.css('div::text').extract_first())
            name = re.sub('\s+', ' ', name)
            url = _strip(div.css('div ::attr(onclick)').extract_first())

            match = re.search("javascript:window.location='(.*?)'", url)
            if not match:
                continue

            url = match.group(1)

            if url and not url.startswith('http'):
                url = response.urljoin(url)

            if url.startswith(base_url):
                yield (name, url)
            else:
                self.logger.info('Check <{}> at <{}>'.format(base_url, url))

        for li in response.css('ul#TocTree li'):
            name = _strip(li.css('a ::text').extract_first())
            name = re.sub('\s+', ' ', name)
            url = _strip(li.css('a ::attr(href)').extract_first())

            if url and not url.startswith('http'):
                url = response.urljoin(url)

            if url.startswith(base_url):
                yield (name, url)
            else:
                self.logger.info('Check <{}> at <{}>'.format(base_url, url))


def _strip(string, default=None):
    if string:
        return string.strip()
    return default


def _normalize(string):
    string = re.sub('\s+', ' ', string.strip().lower())

    return string


def _safe_filename(filename):
    keepcharacters = (' ', '.', '_', '-')
    filename = "".join(c for c in filename if c.isalnum() or c in keepcharacters).rstrip()

    return filename
