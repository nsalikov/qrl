# -*- coding: utf-8 -*-
import re
import scrapy


class ProductsSpider(scrapy.Spider):
    name = 'products'
    allowed_domains = ['qrl.dell.com']
    start_urls = ['https://qrl.dell.com/Product/Categories']


    def parse(self, response):
        urls = response.css('div.CategoryBlock a ::attr(href)').extract()

        for url in urls:
            yield response.follow(url, callback=self.parse_category)


    def parse_category(self, response):
        urls = response.css('ul.ul_ProductsCategory li a ::attr(href)').extract()

        for url in urls:
            yield response.follow(url, callback=self.parse_item)


    def parse_item(self, response):
        d = {}

        d['product'] = _strip(response.css('div#productPicHolder span.Span_productName ::text').extract_first())
        d['product_url'] = response.url

        d['videos'] = self.get_videos(response)
        d['manuals'] = self.get_manuals(response)

        yield d


    def get_videos(self, response):
        results = []

        lis = response.css('div#Videos ul#ProductList li.li_product_video')

        for li in lis:
            name = _strip(li.css('td.li_td_product_video_title center.product_video_title_alignment ::text').extract_first())
            url = None

            for i in li.css('input'):
                _id = i.css('input ::attr(id)').extract_first()
                value = i.css('input ::attr(value)').extract_first()
                if 'videoThumbNailHidden' in _id:
                    url = _strip(value)

            if url and not url.startswith('http'):
                url = response.urljoin(url)

            d = {'name': name, 'url': url}

            results.append(d)

        return results


    def get_manuals(self, response):
        results = []

        lis = response.css('div#Documents ul#ProductList1 li.li_product_document')

        for li in lis:
            name = _strip(li.css('td.td_product_document_alignment center.span_product_document_name ::text').extract_first())
            url = _strip(li.css('td.td_product_document_alignment a ::attr(href)').extract_first())

            if url and not url.startswith('http'):
                url = response.urljoin(url)

            d = {'name': name, 'url': url}

            results.append(d)

        return results


def _strip(string, default=None):
    if string:
        return string.strip()
    return default
