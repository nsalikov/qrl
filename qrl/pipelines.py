# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import scrapy
from scrapy.pipelines.images import FilesPipeline
from scrapy.exceptions import DropItem


class CustomFilePipeline(FilesPipeline):

    def file_path(self, request, response=None, info=None):
        original_path = super().file_path(request, response=None, info=None)
        sha1_and_extension = original_path.split('/')[1] # delete 'full/' from the path

        filename = request.meta.get('filename', '')

        if not filename:
            filename = sha1_and_extension

        return filename


    def get_media_requests(self, item, info):
        meta = {'filename': item['filename']}
        return [scrapy.Request(url, meta=meta) for url in item.get(self.files_urls_field, [])]